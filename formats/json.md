> **IMPORTANT:** This format currently no longer exists! The entry exists nevertheless to get an overview of the format and to document the development properly.

```json3
{
  /* Optional
  "bg": {                // optional bg
    "w": 1000,           // width
    "zw1": 12,           // zwidth1
    "zw2": 12,           // zwidth2
    "sw": 37,            // shadowsize width
    "sh": 9,             // shadowsize height
    "sp": "",            // shadowpath (optional, else default shadow)
    "layers": [          
      {                  
        "p": "test",         // one of the two 
        "c": "04440",        // one of the two 
        "x": 0,              // x position
        "y": 0,              // y position
        "w": 0,              // width
        "h": 0               // height
      }                  
    ]                    
  },                     
  */
  "players": {
    "0": "Luigi600",
    "2": "froze92"
  },
  "author": "Luigi600",
  "difficulty": -1,
  "mode": -1,
  "scenes": [
    {
	  "phase": 0,
	  "phaseCount": 12,
	  "stage": 0,
	  "bound": 0
      "objects": [
        {
// I guess type is not necessary for the player, because the converter "deletes" already all unnecessary parameters
          "oid": 0,     // the object number (memory, 0-400)  -  to follow an object/player in the player
		  "id": 0,		// the data id
		  "hp": 0,      // WHEN CHARACTER then hp
		  "hpD": 0,     // WHEN CHARACTER then dark hp
		  "hpM": 0,     // WHEN CHARACTER then max hp
		  "mp": 0,      // WHEN CHARACTER then mp
		  "hpL": 0,     // WHEN CHARACTER then hp lost
		  "ta": 0,      // WHEN CHARACTER then hp lost
		  "kills": 0,   // WHEN CHARACTER then kills
		  "mpU": 0,     // WHEN CHARACTER mp usage
		  "wp": 0, 		// WHEN CHARACTER weapon picks
		  "shake": 0,   // WHEN CHARACTER AND != 0 ; perhaps necessary for nice debug stuff, so don't add to pos
          "pic": 0,     // the pic number
          "frame": 0,   // the frame number, only necessary for viewer v2.0
          "x": 0,       // x position + centerX (!)
          "y": 0,       // y position + z position + centerY (!)
          "z": 0,       // z position + center_y (!) to draw the shadow
		  "team": 0     // team of the object
        }
      ]
    }
  ]
}
```