# Binary Format of "New LF2 Recording Format" (`nlfr`)

This format does not contain any inputs but pure data for display. Furthermore, few information that are useful in
retrospect, such as the frame ID or kills.

A scene/snapshot is the representation of a complete attack scene. This is similar to the scene while pressing pause.
However, this scene is not standalone. A scene always references the previous scene and overwrites only the values that
have changed. This behavior is comparable
to [video encoding](https://en.wikipedia.org/wiki/Video_compression_picture_types). The first scene is completely
self-contained and independent. This corresponds to the so-called I-frame in video encoding. All further scenes
reference the previous scene, which means that they can be interpreted as P-frame. By referencing the previous scene, it
is not possible to read a single scene directly from the file, but this makes the output signifier smaller. However,
after reading the entire file and processing it, it is possible to pick out a specific scene after all. Thus, it is also
possible to jump back and forth. A feature that is missing in LF2.

The original structure was intended to be in JSON, so the best way to understand the structure of the binary format is
to use JSON as an example. For this reason, there is an "example" chapter for each object with JSON as visualization.

Basically, the binary format is structured so that first comes an attribute name, followed by a value. This value has a
fixed length depending on the attribute or is ended with `0xFF` (see "Special Meaning"). *Flags* can be set in special
objects. These do not have a value. I.e. if this value is set or read, it is effective.



<br />
<br />



## Special Meaning

First it must be clarified that there are bytes which have a special function. When you read values, you do not
always know directly how long the value is. This is especially with strings and with arrays (a list of objects).

| Value | Description                                     |
|-------|-------------------------------------------------|
| 0xFF  | END OBJECT / END ARRAY                          |
| 0x5C  | "\\" ignore next byte meaning, like ignore 0xFF |

`0xFF` is also more common with arrays. With the first `0xFF` the object is terminated, comparable to `}` in JSON. With
a following `0xFF` the array is closed, thus a `]` in JSON. Thus, this byte ensures that various objects (arrays
included) are terminated.



<br />
<br />

## Root Attributes

The "root attributes" are comparable to the JSON root. They define the topmost attributes of an object. In these, for
example, the author and the like are named. This information is important for recording, but does not change during
recording.

| Value | Length         | Remark   | Attr Name      | Description                                                                                             |
|-------|----------------|----------|----------------|---------------------------------------------------------------------------------------------------------|
| 0x01  | 1 (uchar)      |          | bgIndex        | Defines the background **index** (NOT the ID)                                                           |
| 0x02  | - (until 0xFF) | optional | bg             | Defines the complete background (optional, either the index is set or the background is defined)        |
| 0x03  | - (until 0xFF) |          | version        | Defines the version of the recording format. Important in case more information is added in the future  |
| 0x04  | - (until 0xFF) |          | author         | Defines the author of the recording. This information is taken from LF2                                 |
| 0x05  | 1 (char)       |          | difficulty     | Defines the difficulty level in LF2 type (see AI)                                                       |
| 0x06  | 1 (char)       |          | mode           | Defines the mode in LF2 type (see AI). So whether VS, Stage, etc.                                       |
| 0x07  | - (until 0xFF) | array    | players        | Defines the list of (human) players (see Players)                                                       |
| 0x08  | - (until 0xFF) | array    | scenes         | Defines a list of scenes or snapshots                                                                   |
| 0x09  | - (until 0xFF) | array    | data           | Defines the data information necessary for the replay (center X/Y etc.)                                 |
| 0x0A  | - (until 0xFF) |          | recordingInfo  | Defines the recording information. This information is taken from LF2                                   |

<!--
| -     | - (until 0xFF) | array    | binaryDatabase | Defines a "binary database" (see Binary Database)                                                       |
-->


### Example

```json5
{
  "bgIndex": 0,
  "version": "0.0.1",
  "author": "Luigi600",
  "difficulty": 2,
  "mode": 2,
  "players": [
    // ... (see Players)
  ],
  "scenes": [
    // ... (see Scene / Snapshot)
  ],
  "data": [
    // ... (see Data List)
  ],
  "recordingInfo": "The TEAM must PRACTICE"
}
```

<br />
<br />

## General Attributes Start

In binary format, attribute labels occur more frequently now and then. For this reason there are certain bytes that
define them and are valid across a root object.

| Value | Length         | Attr Name          | Description                                              |
|-------|----------------|--------------------|----------------------------------------------------------|
| 0x20  | 4 (int)        | dataID             | Defines a data ID                                        |
| 0x21  | 2 (ushort)     | frameID / binaryID | Defines a frame ID / binary ID                           |
| 0x22  | 4 (int)        | x                  | Defines the x-coordinate of an object                    |
| 0x23  | 4 (int)        | y                  | Defines the y-coordinate of an object                    |
| 0x24  | 4 (int)        | z                  | Defines the z-coordinate of an object                    |
| 0x25  | - (until 0xFF) | name               | Defines a name, like player name or background name.     |
| 0x26  | - (until 0xFF) | path               | Defines a path to a file, like to a data file or sprite. |
| 0x27  | 4 (int)        | width              | Defines a width, such as for a sprite.                   |
| 0x28  | 4 (int)        | height             | Defines a height, such as for a sprite.                  |

**Important:** The binary database uses "frame ID" as "binary ID" (see Binary Database).



<br />
<br />



## Background

Defines the entire background.

**Remark:** This field is optional and should only be used if a modified background is present. Otherwise `bgIndex`
should be used!

| Value       | Length                   | Remark | Attr Name    | Description        |
|-------------|--------------------------|--------|--------------|--------------------|
| 0x30        | 4 (int)                  |        | zBoundary1   | z boundary 1       |
| 0x31        | 4 (int)                  |        | zBoundary2   | z boundary 2       |
| 0x32        | 2 (short)                |        | shadowWidth  | shadow size width  |
| 0x33        | 2 (short)                |        | shadowHeight | shadow size height |
| 0x34        | - (until 0xFF)           |        | shadowPath   | shadow path        |
| 0x35        | - (until 0xFF)           | array  | layers       | layers             |
|             |
| path, width | (see General Attributes) |

### Layer

Defines a layer of a background.

| Value                     | Length                   | Remark               | Attr Name | Description |
|---------------------------|--------------------------|----------------------|-----------|-------------|
| 0x36                      | - (until 0xFF)           | optional             | color     | color       |
| 0x37                      | 4 (int)                  | optional, default 0  | loop      | loop        |
| 0x38                      | 4 (int)                  | optional, default -1 | c1        | c1          |
| 0x39                      | 4 (int)                  | optional, default -1 | c2        | c2          |
| 0x3A                      | 4 (int)                  | optional, default -1 | cc        | cc          |
|                           |                          |                      |           |             |
| x, y, width, height, path | (see General Attributes) |

**Flags**

| Value | Length | Remark                         | Attr Name    | Description         |
|-------|--------|--------------------------------|--------------|---------------------|
| 0x3F  | 0      | default 0 (false) (if not set) | transparency | transparency (if 1) |

### Example

```json5
{
  // ...
  "background": {
    "name": "Brokeback Clif",
    "width": 1500,
    "zBoundary1": 300,
    "zBoundary2": 510,
    "shadow": "bg\sys\bc\s.bmp",
    "shadowWidth": 37,
    "shadowHeight": 9,
    "layers": [
      {
        "path": "bg\sys\bc\bc1.bmp",
        "x": 0,
        "y": 129,
        "transparency": true
      }
      // ...
    ]
  }
  // ...
}
```



<br />
<br />



## Scene / Snapshot

Defines a scene / snapshot that represents a state of the game. Except the first scene, the scenes always reference
the previous one. Thus, only the very first one is complete and independent. All other scenes overwrite the previous 
scene by writing out the changed information.

| Value | Length         | Remark           | Attr Name  | Description                 |
|-------|----------------|------------------|------------|-----------------------------|
| 0x50  | 2 (ushort)     | optional         | phase      | Phase                       |
| 0x51  | 2 (ushort)     | optional         | phaseCount | Phase Count                 |
| 0x52  | 1 (uchar)      | optional         | stage      | Stage                       |
| 0x53  | 2 (short)      | optional         | bound      | Bound                       |
| 0x5E  | - (until 0xFF) | optional & array | sparks     | "Spark"-Effects (Hit/Blood) |
| 0x5F  | - (until 0xFF) | array            | objects    | Objects                     |

### Object

Defines an object that represents a character, weapon, "broken particles" or similar on the game field.
To create a new object that does not exist in the previous scene, all attributes must be written out. This case also 
occurs if the object has got the same ID from LF2 as an object that already exists and thus replaces the old one (A 
special case when LF2 wants to create more objects than it can).

`objectID` is necessary in every scene. If the ID is not written out and an object with this ID existed in the previous 
scene, it will be deleted.

| Value                      | Length                   | Remark                                              | Attr Name   | Description                 |
|----------------------------|--------------------------|-----------------------------------------------------|-------------|-----------------------------|
| 0x60                       | 2 (ushort)               | required (EXISTS ALWAYS, ALSO IN REFERENCE SCENES!) | objectID    | Object ID                   |
| 0x61                       | 1 (char)                 |                                                     | team        | Team                        |
| 0x62                       | 2 (short)                |                                                     | hp          | HP                          |
| 0x63                       | 2 (short)                | only when character                                 | mp          | MP                          |
| 0x64                       | 2 (short)                |                                                     | invisible   | Invisible, exists when != 0 |
| 0x65                       | 2 (short)                |                                                     | blink       | blink, exists when != 0     |
| 0x66                       | 2 (short)                |                                                     | shake       | Shake (exists when != 0)    |
| 0x67                       | 2 (short)                | only when character                                 | hpDark      | HP Dark                     |
| 0x68                       | 2 (short)                | only when character                                 | hpMax       | HP Max                      |
| 0x69                       | 4 (int)                  | only when character                                 | hpLost      | HP Lost                     |
| 0x6A                       | 4 (int)                  | only when character                                 | totalAttack | Total Attack                |
| 0x6B                       | 2 (short)                | only when character                                 | kills       | Kills                       |
| 0x6C                       | 4 (int)                  | only when character                                 | mpUsage     | MP Usage                    |
| 0x6D                       | 2 (ushort)               | only when character                                 | weaponPicks | Weapon Picks                |
|                            |
| x, y, z, data ID, frame ID | (see General Attributes) |

**Flags**

| Value | Length | Remark    | Attr Name  | Description                                         |
|-------|--------|-----------|------------|-----------------------------------------------------|
| 0x7C  | 0      | default 0 | showShadow | Show Shadow (just write out when by changes)        |
| 0x7D  | 0      | default 0 | hideShadow | Hide Shadow                                         |
| 0x7E  | 0      |           | facing     | None Facing (if 0, just write out when by changes)  |
| 0x7F  | 0      |           | facing     | Facing (if 1)                                       |

### "Spark"-Effect

Defines the so-called spark effect are small sprites that appear during a hit. This includes a normal hit-smash in small 
and large, as well as a blood effect in small and large.

**Important:** Only the first frame is written out. The rest is "calculated" automatically by a viewer. I.e. the
position and the type of the "Spark" remains the same. This allows the user to display it as in the original LF2 (i.e.
it is animated even though there is a pause) or to be affected by the pause.

| Value          | Length                   | Description |
|----------------|--------------------------|-------------|
|                |
| x, y, Frame ID | (see General Attributes) |

### Example

```json5
{
  "phase": 5,
  "phaseCount": 10,
  "stage": 6,
  "bound": 1600,
  "sparks": [
    {
      "frameID": 0,
      "x": 117,
      "y": 532,
    }
  ],
  "objects": [
    // ... the objects overwrite only the changed values of the previous scene(s)
    // an object in the first scene looks very similar, with almost everything written out (i.e. position, frame ID, stats, team, etc.)
    {
      "objectID": 50,
      "dataID": 210,
      "frameID": 5,
      "x": 12,
      "y": -30,
      "z": 300,
      "hp": 500,
      "team": 2,
    },
    {
      "objectID": 0,
      "dataID": 1,
      "frameID": 212,
      "team": 0,
      "hp": 371,
      "mp": 500,
      "blink": 4,
      "hpDark": 380,
      "hpMax": 500,
      "hpLost": 1251,
      "totalAttack": 3216,
      "kills": 126,
      "mpUsage": 1025,
      "weaponPicks": 15
    }
  ]
}
```



<br />
<br />



## Data List

Defines a data list that contains all datas and frames that occur during the recording. However, these datas are only a 
fraction of information than the actual dat file. This allows to view the records without having to read data files. 
Furthermore, it supports minor changes to the data files.

| Value   | Length                   | Description |
|---------|--------------------------|-------------|
| 0x80    | - (until 0xFF 0xFF)      | Frames      |
|         |
| Data ID | (see General Attributes) |

### Frame

Defines a frame that occurred during the recording of the data object. Only the minimum information is written out, 
which is necessary for the display.

| Value    | Length                   | Description |
|----------|--------------------------|-------------|
| 0x90     | 2 (ushort)               | pic         |
| 0x91     | 2 (short)                | Center X    |
| 0x92     | 2 (short)                | Center Y    |
|          |
| Frame ID | (see General Attributes) |

### Example

```json5
{
  // ...
  "dataList": [
    {
      "dataID": 6,
      "frames": [
        {
          "frameID": 5,
          "pic": 5,
          "centerX": 37,
          "centerY": 79
        }
      ]
    },
    {
      "dataID": 52,
      "frames": [
        {
          "frameID": 5,
          "pic": 5,
          "centerX": 37,
          "centerY": 109
        }
      ]
    },
    // ...
  ]
  // ...
}
```



<br />
<br />



## Players

Defines a list of (active) players that appear during the recording.

| Value | Length                   | Remark                 | Attr Name | Description |
|-------|--------------------------|------------------------|-----------|-------------|
| 0xA0  | 1 (uchar)                | different type length! | objectID  | Object ID   |
|       |
| name  | (see General Attributes) |

### Example

```json5
{
  // ...
  "players:": [
    {
      "objectID": 0,
      "name": "mfc"
    },
    {
      "objectID": 1,
      "name": "Silverthorn"
    },
    {
      "objectID": 2,
      "name": "Luiig600"
    }
  ]
  // ...
}
```

<br />
<br />


<!--

// CHANGED (ALL!)
## Objects

| Value               | Length                   | Description |
|---------------------|--------------------------|-------------|
| 0xC1                | 1 (uchar)                | Col         |
| 0xC1                | 1 (uchar)                | Row         |
|                     |
| path, width, height | (see General Attributes) |

-->
