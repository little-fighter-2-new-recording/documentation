# Little Fighter 2 New Recording Format

This includes simple documentation about the output format "nlfr" (New Little Fighter Recording Format).

At the beginning of the development JSON was planned, however the size of the format was still too large. For this reason, JSON was replaced by a simple binary format during the development.

## Formats

 * [Binary](formats/binary.md)
 * [~~JSON~~](formats/json.md)